import React, { useState, useEffect } from "react";
import styled from "styled-components";
// utils
import { getFromLS } from "utils/storage";
import { useTheme } from "./useTheme";
import { CheckOutlined } from "@ant-design/icons"

const ThemedButton = styled.button`
  border: 0;
  display: inline-block;
  font-size: 14px;
  border-radius: 4px;
  margin-top: 5px;
  width: 25px;
  height: 25px;
  cursor: pointer;
  border: 1px solid #e4e4e4;
  svg {
    color: #ffffff;
    line-height: 25px;
    display: inline-block;
    vertical-align: middle;
  }
`;

const Wrapper = styled.li`
  text-align: center;
  list-style: none;
`;

const Container = styled.ul`
  display: grid;
  gap: 5px;
  grid-template-columns: repeat(5, 1fr);
  width: 100%;
`;

const ThemeSelector = ({ changeTheme, setter }) => {
  const { data } = getFromLS("all-themes");
  const [themes, setThemes] = useState([]);
  const { setMode, theme: selected } = useTheme();
  const themeSwitcher = (selectedTheme) => {
    changeTheme(selectedTheme);
    setMode(selectedTheme);
    setter(selectedTheme);
  };

  useEffect(() => {
    setThemes(Object.keys(data));
  }, []);

  const ThemeCard = (props) => {
    return (
      <Wrapper>
        <ThemedButton
          onClick={() => themeSwitcher(props.theme)}
          style={{
            backgroundColor: `${data[props.theme.name].colors.button.background}`,
            color: `${data[props.theme.name].colors.button.text}`,
          }}
        >
          {props.isSelected && <CheckOutlined />}
        </ThemedButton>
      </Wrapper>
    );
  };
  return (
    <div>
      <Container>
        {themes?.length > 0 &&
          themes.map((theme) => {
            const isSelected = theme === selected.name;
            return <ThemeCard theme={data[theme]} key={data[theme].id} isSelected={isSelected} />;
          })}
      </Container>
    </div>
  );
};
export default (ThemeSelector);
