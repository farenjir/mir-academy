import { getFromLS, setToLS } from "utils/storage";
// languages
import en_lang from "./en_lang"
import fa_lang from "./fa_lang"
// languagesObj
let languages = { en: en_lang, fa: fa_lang }

const selectedLang = getFromLS("language");
if (!selectedLang) setToLS("language", "fa") // default language is FA


let language = languages?.[selectedLang] || fa_lang

export default language



// DES: we can use "i18next" & "i18next-browser-languagedetector" packages for handle languages