const en_lang = {
  author: "Farshid Enjili",
  message: { requiredMessage: "This field is required", videoNotSupport: "Your browser is not supported!" },
  errors: {},
  loading: "LOADING ....",
  theme: {
    changeMenuPosition: "change Menu Position"
  },
  notFound: {
    notFound: "The requested page could not be found",
    goBack: "Return to the home page"
  },
  layouts: {
    header: {
      title: "Central Perk",
      subTitle: "is where life happens",
      des: "Discover our tours",
      navigation: {}

    },
    mainFooter: {
      builtBy: " Built by "
      , describeDev: "  . Copyright by Farshid Enjili. Web programmer and startup activist. So far, I have worked in two startup teams (Serve Information Technology and Fitibody) and also worked as a freelancer. For more than a year now, I have been working as a front-end developer (javascrtipt/react) in Farafan Future Wave Company."
    }
    , popup: {
      title: "Start booking now",
      link: "Book now",
      des: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua
                Sed sed risus pretium quam.Aliquam sem et tortor consequat id.Volutpat odio facilisis mauris sit
                amet massa vitae.Mi bibendum neque egestas congue.Placerat orci nulla pellentesque dignissim enim
                sit.Vitae semper quis lectus nulla at volutpat diam ut venenatis.Malesuada pellentesque elit eget
                gravida cum sociis natoque penatibus et.Proin fermentum leo vel orci porta non pulvinar neque laoreet.
                Gravida neque convallis a cras semper.Molestie at elementum eu facilisis sed odio morbi quis.Faucibus
                vitae aliquet nec ullamcorper sit amet risus nullam eget.Nam libero justo laoreet sit.Amet massa
                vitae tortor condimentum lacinia quis vel eros donec.Sit amet facilisis magna etiam.Imperdiet sed
                euismod nisi porta.`,
      subTitle: "Important Please read these terms before booking"
    },

  },
  mainPages: { home: { title: "Home Page" } },
  mockLorem1: `Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam, ipsum sapiente aspernatur
  libero repellat quis consequatur ducimus quam nisi exercitationem omnis earum qui. Aperiam,
  ipsum sapiente aspernatur libero repellat quis consequatur ducimus quam nisi exercitationem
  omnis earum qui.`,
  mockLorem2: `Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam, ipsum sapiente aspernatur
  libero repellat quis consequatur ducimus quam nisi exercitationem omnis earum qui.`,
  mockLorem3: `Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam.`
}

export default en_lang