import * as main from "./pages/main"
import * as  other from "./pages/other"


const mainPages = [
  {
    name: "Home",
    breadcrumbName: "Home",
    exact: true,
    component: main.HomePage,
    path: "/",
  },
  {
    name: "Login",
    breadcrumbName: "Login",
    exact: true,
    component: main.Login,
    path: "/login",
  },
  {
    name: "Register",
    breadcrumbName: "Register",
    exact: true,
    component: main.Register,
    path: "/register",
  },
]

const dashboardPages = []

const otherPages = [
  {
    name: "NotFoundPage",
    breadcrumbName: "NotFound",
    exact: true,
    component: other.NotFoundPage,
    path: "*",
  },
]


const routes = [...mainPages, ...dashboardPages, ...otherPages]

export default routes