export { default as HomePage } from 'modules/main/home';
export { default as Login } from 'modules/main/login';
export { default as Register } from 'modules/main/register';

