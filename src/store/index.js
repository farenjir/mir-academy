import { combineReducers } from "redux";
import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
// middleware
import { configureStore, getDefaultMiddleware } from "@reduxjs/toolkit";
// utils
import { createLogger } from "redux-logger";
// states
import { cellMatrixInfoReducer } from "./states/users/slice";

let middleware = [];
const logger = createLogger();

if (process.env.NODE_ENV === "development") {
  middleware = [
    ...getDefaultMiddleware({
      serializableCheck: false,
    }),
    logger,
  ];
} else {
  middleware = [
    ...getDefaultMiddleware({
      serializableCheck: false,
    }),
  ];
}

const reducers = combineReducers({
  cellMatrixInfoReducer,
});

const persistConfig = {
  key: "root",
  storage,
};

const persistedReducer = persistReducer(persistConfig, reducers);

const store = configureStore({
  reducer: persistedReducer,
  middleware,
  devTools: process.env.NODE_ENV === "development",
});

export default store;
