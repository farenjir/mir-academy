import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  cellData: {},
};

const cellMatrixInfo = createSlice({
  name: "cellMatrixData",
  initialState,
  reducers: {
    cellData: (state, action) => {
      state.cellData = action.payload;
    },
  },
});

export const cellMatrixInfoReducer = cellMatrixInfo.reducer;
export const { cellData } = cellMatrixInfo.actions;
