import { cellData } from "./slice";

export const saveCellMatrixData = (item) => async (dispatch) => {
  return await dispatch(cellData(item));
};
