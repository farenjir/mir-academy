import React, { useContext } from "react";
import UserContext from "utils/oidc-context";
import StyledPageTitle from "./style";

const PageTitle = ({ classes, title, ...props }) => {
  const { theme } = useContext(UserContext);
  return (
    <StyledPageTitle themeType={theme}>
      <div className={`${classes} pages-title-wrapper`}>
        <h1 className="pages-title">{title}</h1>
      </div>
    </StyledPageTitle>
  )
};
export default (PageTitle);