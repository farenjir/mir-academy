import styled from "styled-components";
const StyledPageTitle = styled.div`
    .pages-title-wrapper {
      .pages-title{
        color: ${props => props.themeType?.colors?.pageTitle};
        &:after {
          background-color: ${props => props.themeType?.colors?.pageTitle};
        }
      }
    }
`;
export default (StyledPageTitle);