import styled from "styled-components";
import { Table } from "antd";

const StyledTable = styled(Table)`
  ${(props) =>
    props.selectedTheme &&
    `
      .ant-table-thead .ant-table-cell{
        background-color: ${props.selectedTheme?.colors?.table?.header} !important;
        }
    
      .ant-table-cell-fix-left, 
      .ant-table-cell-fix-right {
        background-color: ${props.selectedTheme?.colors?.table?.header} !important;
        }

        .ant-table .ant-table-tbody .ant-table-row:nth-child(odd) {
          background-color: ${props.selectedTheme?.colors?.table?.firstRow} !important;
          font-size: 30px;
        }
        .ant-table .ant-table-tbody .ant-table-row:nth-child(even) {
          background-color: ${props.selectedTheme?.colors?.table?.secondRow} !important;
          font-size: 30px;
        }
  `}
`;
export default StyledTable;
