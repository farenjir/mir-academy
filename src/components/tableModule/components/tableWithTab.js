import React, { useEffect, useState } from "react";
import { Tabs, Row, Col } from "antd";
import { getFromLS } from "utils/storage";
import StyledTab from "./style";
import { TableModule } from "components";

const { TabPane } = Tabs;

const TableWithTab = ({
  tableTabs,
  initTable,
  spanTable = 24,
  defaultActiveKey,
  contentRender,
  onChangeTab,
  exportRef,
  ...props
}) => {
  const [tabs, setTabs] = useState([]);
  const selectedTheme = getFromLS("theme");
  const renderComponent = (record) => {
    if ("conditions" in record) {
      return record.conditions ? true : false;
    }
    return true;
  };
  useEffect(() => {
    tableTabs && setTabs(tableTabs);
  }, [tableTabs]);
  return (
    <StyledTab
      selectedtheme={selectedTheme}
      defaultActiveKey={defaultActiveKey}
      onChange={props.onChange}
      size="large"
      type="card"
    >
      {tabs &&
        tabs?.tabs?.map((item, index) => {
          if (renderComponent(item)) {
            return (
              <TabPane
                tab={item?.title}
                key={item?.parameter ? item?.parameter : item?.title}
                onChange={onChangeTab}
              >
                <Row>
                  <Col span={spanTable}>
                    <TableModule
                      exportRef={exportRef}
                      columns={item?.columns}
                      data={item?.data}
                      initTable={initTable}
                      searchOnColumn={item?.searchOnColumn}
                    />
                  </Col>
                </Row>
              </TabPane>
            );
          }
          return "finish";
        })}
    </StyledTab>
  );
};

export default TableWithTab;
