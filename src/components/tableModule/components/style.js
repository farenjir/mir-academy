import styled from "styled-components";
import { Tabs } from "antd";

const StyledTab = styled(Tabs)`
  ${(props) =>
    props.selectedtheme &&
    ` &.ant-tabs{
      .ant-tabs-nav{
        margin-bottom:35px;
        .ant-tabs-tab.ant-tabs-tab-active{
          .ant-tabs-tab-btn{
            color: ${props.selectedtheme?.colors?.tabs?.color};
            &:hover{
              color: ${props.selectedtheme?.colors?.tabs?.hoverText};
            }
          }
        }
        .ant-tabs-ink-bar{
          background:${props.selectedtheme?.colors?.tabs?.borderBg};
        }
      }
    }
  `}
`;
export default StyledTab;
