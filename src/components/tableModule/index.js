// import Highlighter from 'react-highlight-words';
import React from "react";
// style
import { Input, Button, Space, Row, Col, Select } from "antd";
import { CaretLeftFilled, SearchOutlined } from "@ant-design/icons";
import StyledTable from "./style";
import { getFromLS } from "utils/storage";

class TableModule extends React.Component {
  state = {
    filteredColumn: [],
    searchText: "",
    data: [],
    columns: [],
    initTable: {
      scroll: undefined,
      expandable: false,
      hasData: true,
      showHeader: true,
      footer: false,
      rowSelection: false,
      title: undefined,
      tableLayout: undefined,
      top: "none",
      bottom: "bottomLeft",
    },
    loading: true,
  };
  getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }) => (
      <div className="p-2">
        <Input
          ref={(node) => {
            this.searchInput = node;
          }}
          placeholder={""}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() =>
            this.handleSearch(selectedKeys, confirm, dataIndex)
          }
          className="mb-2 d-xl-block d-lg-block"
        />
        <Space>
          <Button
            type="primary"
            onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
          >
            {this.props?.t("search")}
          </Button>
          <Button
            onClick={() => this.handleReset(clearFilters)}
            size="small"
            style={{ width: 90 }}
          >
            {this.props?.t("reset")}
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        ? record[dataIndex]
          .toString()
          .toLowerCase()
          .includes(value.toLowerCase())
        : "",
    onFilterDropdownVisibleChange: (visible) => {
      if (visible) {
        setTimeout(() => this.searchInput.select(), 100);
      }
    },
  });
  handleSearch = (selectedKeys, confirm, dataIndex) => {
    const { handleSearchOnColumn } = this.props;
    handleSearchOnColumn &&
      handleSearchOnColumn(selectedKeys, confirm, dataIndex);
    confirm();
    this.setState({
      searchText: selectedKeys[0],
      searchedColumn: dataIndex,
    });
  };
  handleReset = (clearFilters) => {
    const { handleSearchReset } = this.props;
    handleSearchReset && handleSearchReset(clearFilters);
    clearFilters();
    this.setState({ searchText: "" });
  };
  // setTableData
  setTableData = () => {
    const { columns, data, initTable, searchOnColumn } = this.props;
    const columnsData = [];
    this.setState({ loading: true });
    columns &&
      columns.map((column) => {
        if (searchOnColumn && column?.children) {
          // start >> makeNewColumn with Search for Children Columns
          const childrenColumn = [];
          column.children.map((child) => {
            if (searchOnColumn.find((item) => item === child?.dataIndex)) {
              childrenColumn.push({
                ...child,
                ...this.getColumnSearchProps(`${child.dataIndex}`),
              });
            } else {
              childrenColumn.push({
                ...child,
              });
            }
            return "finish";
          });
          delete column?.children;
          column["children"] = childrenColumn;
          columnsData.push({
            ...column,
          });
          // end >> makeNewColumn with Search for Children Columns
        } else {
          if (
            searchOnColumn &&
            searchOnColumn.find((item) => item === column?.dataIndex)
          ) {
            columnsData.push({
              ...column,
              ...this.getColumnSearchProps(`${column.dataIndex}`),
            });
          } else {
            columnsData.push({
              ...column,
            });
          }
        }
        return this.setState({
          columns: columnsData,
          data,
          initTable: { ...this.state.initTable, ...initTable },
          loading: false,
        });
      });
  };
  filteredColumns = (filteredColumn) => {
    const { columns } = this.state;
    const { fixedColumn } = this.props;
    const allFixedColumn = fixedColumn
      ? [...fixedColumn.fixLeft, ...fixedColumn.fixRight]
      : [];
    const fixColumns = (rightLeft) =>
      fixedColumn &&
      fixedColumn[rightLeft]
        .map((item) => columns.find((column) => column?.dataIndex === item))
        .filter((obj) => obj !== undefined);

    // setDATA
    const columnsData =
      columns &&
      columns
        .filter(
          (item) =>
            item?.dataIndex !==
            allFixedColumn.find((record) => record === item?.dataIndex)
        )
        .filter(
          (obj) =>
            obj.dataIndex !==
            filteredColumn.find((record) => record === obj?.dataIndex)
        );

    this.setState({ loading: true });
    return this.setState({
      filteredColumn: [
        ...fixColumns("fixRight"),
        ...columnsData,
        ...fixColumns("fixLeft"),
      ],
      loading: false,
    });
  };
  handleChangeFiltered = (value) => {
    this.filteredColumns(value);
  };

  // setDATA
  componentDidMount() {
    return this.setTableData();
  }
  componentDidUpdate(preProps) {
    const { filteredColumn, data } = this.props;
    if (preProps.filteredColumn !== filteredColumn) {
      return this.filteredColumns(filteredColumn);
    }
    if (preProps?.data !== data) {
      return this.setTableData();
    }
  }
  render() {
    const { columns, data, loading, initTable, filteredColumn } = this.state;
    const { filterColumn, t, onChange, fixedColumn } = this.props;
    const allFixedColumn = fixedColumn
      ? [...fixedColumn.fixLeft, ...fixedColumn.fixRight]
      : [];
    const selectedTheme = getFromLS("theme");
    return (
      <>
        {filterColumn && (
          <Row>
            <Col xs={24}>
              <section className="filtered-section">
                <div className="d-flex justify-content-start align-items-center">
                  <CaretLeftFilled />
                  <span className="mx-1 my-3">
                    {t("Events.LookEvents.filterEvents")}
                  </span>
                </div>
              </section>
            </Col>
            <Col xs={24} xl={18}>
              <Select
                className="w-100 mb-5"
                mode="multiple"
                allowClear={true}
                showArrow
                onChange={this.handleChangeFiltered}
                optionLabelProp="label"
              >
                {columns &&
                  columns.map((item) =>
                    allFixedColumn ? (
                      allFixedColumn.find(
                        (dataIndex) => dataIndex === item.dataIndex
                      ) ? (
                        ""
                      ) : (
                        <Select.Option
                          value={item.dataIndex}
                          label={item.title}
                        >
                          {item.title}
                        </Select.Option>
                      )
                    ) : (
                      <Select.Option value={item.dataIndex} label={item.title}>
                        {item.title}
                      </Select.Option>
                    )
                  )}
              </Select>
            </Col>
          </Row>
        )}
        <StyledTable
          selectedTheme={selectedTheme}
          rowKey={(record) => record?.key}
          onChange={onChange}
          showSorterTooltip={false}
          columns={filteredColumn?.length > 1 ? filteredColumn : columns}
          dataSource={data}
          loading={loading}
          {...initTable}
        />
      </>
    );
  }
}

export default TableModule
