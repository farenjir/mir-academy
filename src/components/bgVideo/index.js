import React from "react";
import language from "assets/langs";
import "./style.scss";

const BgVideo = ({ video, webVideo }) => {
  return (
    <div className="bg-video">
      <video className="bg-video__content" autoPlay muted loop>
        <source src={video} type="video/mp4" />
        {webVideo && <source src={webVideo} type="video/webm" />}
        <span>{language?.message?.videoNotSupport}</span>
      </video>
    </div>
  );
};

export default BgVideo;
