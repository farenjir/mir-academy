import React from "react";
import "./style.scss";

const CompositionCard = ({ smallImg, name = "p1" }) => {
  return (
    <>
      {smallImg && (
        <img className={`composition__photo composition__photo--${name}`} alt={name} src={smallImg} />
      )}
    </>
  );
};

export default CompositionCard;
