import React from 'react'
import "./style.scss"

const StoryCard = ({ figcaption = "", header = "", description = "", imgSrc = "" }) => {
  return (
    <div className="story">
      <figure className="story__shape">
        <img src={imgSrc} alt={header} className="story__img" />
        <figcaption className="story__caption">{figcaption}</figcaption>
      </figure>
      <div className="story__text">
        <h3 className="heading-tertiary u-margin-bottom-small">{header}</h3>
        <p>
          {description}
        </p>
      </div>
    </div>
  )
}

export default StoryCard