import React from "react";

const InputModule = ({ id = "", type = "text", description = "", placeholder = "", required = false }) => {
  return (
    <>
      <div className="form__group">
        <input type={type} className="form__input" placeholder={placeholder} id={id} required={required} />
        <label for={id} className="form__label">
          {description}
        </label>
      </div>
    </>
  );
};

export default InputModule;
