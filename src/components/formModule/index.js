import React from 'react'
import "./style.scss"

const FormModule = ({ children, title = "", submitTitle = "submit", action = "#" }) => {
  return (
    <>
      <div className="book__form">
        <form action={action} className="form">
          <div className="u-margin-bottom-medium">
            <h2 className="heading-secondary">{title}</h2>
          </div>
          {children}
          <div className="form__group">
            <button className="btn btn--green">{submitTitle}</button>
          </div>
        </form>
      </div>
    </>
  )
}

export default FormModule