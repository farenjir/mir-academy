import React, { Component } from "react";
import { Select, Form } from "antd";
import language from "assets/langs";

const { Option } = Select;

class InputSelect extends Component {
  onClick = (e) => {
    e.stopPropagation();
  };
  render() {
    const {
      label,
      name,
      required,
      children,
      mode,
      onChange,
      disabled,
      classes,
      defaultValue,
      search,
      loading,
      fieldKey,
      restField,
      allowClear,
      placeholder,
    } = this.props;
    return (
      <>
        <Form.Item
          labelCol={{ xs: 24 }}
          wrapperCol={{ xs: 24 }}
          name={name}
          label={label}
          rules={[
            {
              required: required,
              message: language?.message?.requiredMessage,
            },
          ]}
          initialValue={defaultValue}
          fieldKey={fieldKey}
          {...restField}
        >
          <Select
            allowClear={allowClear}
            onChange={onChange}
            mode={mode}
            defaultValue={defaultValue}
            className={`w-100 ${classes}`}
            placeholder={placeholder || ""}
            disabled={disabled}
            onClick={this.onClick}
            showSearch={search}
            loading={loading}
            optionFilterProp="children"
            filterOption={(input, option) =>
              option?.children?.toLowerCase()?.indexOf(input?.toLowerCase()) >=
              0
            }
            getPopupContainer={(trigger) => trigger.parentNode}
          >
            {children?.length > 0 &&
              children.map((item, index) => {
                return (
                  <Option value={item?.id} key={index}>
                    {item?.name}
                  </Option>
                );
              })}
          </Select>
        </Form.Item>
      </>
    );
  }
}


export default InputSelect
