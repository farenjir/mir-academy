import React from 'react'
import "./style.scss"

const FeatureBox = ({ header = "", description = "", icon = "", iconColor = "" }) => {
  return (
    <div className="feature-box">
      <span className={`feature-box__icon feature-box__icon-${iconColor}`}>{icon}</span>
      <h3 className="heading-tertiary u-margin-bottom-small">{header}</h3>
      <p className="feature-box__text">
        {description}
      </p>
    </div>
  )
}

export default FeatureBox