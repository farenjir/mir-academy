import React, { Component } from "react";
import { Form, Input } from "antd";
import language from "assets/langs";

class FloatLabel extends Component {
  constructor() {
    super();
    this.state = {
      className: "",
      value: null,
      editStyle: null,
    };
  }

  // componentDidMount() {
  //   this.props.disabled && this.setState({ className: "focused" });
  //   !!this.props.edit && this.setState({ className: "focused" });
  //   this.checkInputValue();
  // }

  // componentDidUpdate(prevProps, prevState) {
  //   if (prevState.value !== this.myinput.state.value) {
  //     this.checkInputValue();
  //   }
  // }

  // checkInputValue = () => {
  //   this.setState({ value: this.myinput.state.value }, () => {
  //     if (this.state.value) this.setState({ className: "focused" });
  //   });
  // };

  // onFocusFunction = (e) => {
  //   this.setState({ className: "focused" });
  // };

  // onBlurFunction = (e) => {
  //   e.target.value.length !== 0
  //     ? this.setState({ className: "focused" })
  //     : this.setState({ className: "" });
  // };

  render() {
    const {
      type,
      label,
      name,
      classes,
      onChange,
      value,
      required,
      pattern,
      patternMessage,
      defaultValue,
      placeholder,
      tabIndex,
      disabled,
      maxlength,
      onKeyUp,
      maxNumber,
      minNumber,
      step,
    } = this.props;
    const { className } = this.state;
    return (
      <>
        <Form.Item
          labelCol={{ xs: 24 }}
          wrapperCol={{ xs: 24 }}
          // className={`float-label ${className}`}
          className={`${className}`}
          label={label}
          name={name}
          initialValue={defaultValue}
          rules={[
            {
              required: required,
              message: language?.message?.requiredMessage,
            },
            // {
            //   pattern: Regex.firstSpaceBlock,
            //   message: t("regex.firstSpaceBlock"),
            // },
            {
              pattern: pattern,
              message: patternMessage,
            },
          ]}
        >
          {this.props.type === "password" ? (
            <Input.Password
              ref={(input) => (this.myinput = input)}
              type={type}
              placeholder={placeholder}
              className={`${classes || ""}`}
              onFocus={this.onFocusFunction}
              onBlur={this.onBlurFunction}
              autoComplete="off"
              tabIndex={tabIndex}
            // iconRender={(visible) =>
            //   visible ? (
            //     <FontAwesomeIcon icon={iconTransformer("eye")} />
            //   ) : (
            //     <FontAwesomeIcon icon={iconTransformer("eye-slash")} />
            //   )
            // }
            />
          ) : this.props.type === "textarea" ? (
            <Input.TextArea
              ref={(input) => (this.myinput = input)}
              type={type}
              className={classes || ""}
              onFocus={this.onFocusFunction}
              onBlur={this.onBlurFunction}
              autoComplete="off"
              placeholder={placeholder}
              tabIndex={tabIndex}
            />
          ) : (
            <Input
              onKeyUp={onKeyUp}
              productid={this.props.productid}
              id={this.props.id}
              name={this.props.name}
              ref={(input) => (this.myinput = input)}
              type={type}
              max={maxNumber}
              min={minNumber}
              placeholder={placeholder}
              className={`${classes || ""}`}
              onFocus={this.onFocusFunction}
              onBlur={this.onBlurFunction}
              onChange={onChange}
              value={value}
              defaultValue={defaultValue}
              disabled={disabled}
              maxLength={maxlength}
              autoComplete="off"
              tabIndex={tabIndex}
              step={step || "0.001"}
            // pattern={pattern}
            />
          )}
        </Form.Item>
      </>
    );
  }
}

export default FloatLabel
