import { Button } from "antd";
import styled from "styled-components";

const StyledButton = styled(Button)`
    ${props => props.selectedtheme && `
    &.btn-public{
      height: 38px;
      line-height: 38px;
      border-radius: 3px;
      min-width: 150px;
      background-color: #ffffff;
      text-align: center;
      padding: 0 2rem;
      transition: all 0.3s linear;
      cursor: pointer;
      border:solid 1px transparent;
      &.btn-action {
        box-shadow: -3px 3px 6px 0 rgba(0, 0, 0, 0.16);
        color: ${props.selectedtheme?.colors?.button?.type?.action?.color}; 
        border-color: ${props.selectedtheme?.colors?.button?.type?.action?.borderColor};
        background-color: ${props.selectedtheme?.colors?.button?.type?.action?.bg};
        &:hover {
          color: ${props.selectedtheme?.colors?.button?.type?.action?.hoverText};
          border-color: ${props.selectedtheme?.colors?.button?.type?.action?.hoverBorderColor};
          background-color: ${props.selectedtheme?.colors?.button?.type?.action?.hoverBg};
        }
      }
      &.btn-cancel {
        background-color: #cccccc;
        color: #f7f7f7;
      }
      &.btn-disable {
          color: ${props.selectedtheme?.colors?.button?.type?.disable?.color};
          border-color: ${props.selectedtheme?.colors?.button?.type?.disable?.borderColor};
          cursor:default;
      }
      &.btn-setting {
        left: 0;
        top: 50%;
        z-index: 10;
        display: block;
        position: absolute;
        width: 52px;
        height: 52px;
        min-width: 0;
        line-height: 52px;
        text-align: center;
        cursor: pointer;
        font-size: 25px;
        padding: 4px 13px;
        margin: 0;
        border-radius: 3px;
        transition: background-color .2s;
        color: ${props.selectedtheme?.colors?.button?.type?.setting?.color}; 
        background-color: ${props.selectedtheme?.colors?.button?.type?.setting?.bg};
      }
    }
  `}
`;
export default (StyledButton);