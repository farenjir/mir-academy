import React from "react";
import { getFromLS } from "utils/storage";
import StyledButton from "./style";
// theme
import * as themes from "theme/schema.json";
const ButtonPublic = ({
    children,
    type,
    onClick,
    flavor,
    classes,
    loading,
    htmlType,
    disabled,
    border,
    ...props
}) => {
    // const selectedTheme = getFromLS("theme");
    const selectedTheme = themes?.default;
    return (
        <StyledButton
            selectedtheme={selectedTheme}
            htmlType={htmlType}
            loading={loading}
            type={type}
            onClick={onClick}
            disabled={disabled}
            border={border}
            className={`btn-public btn-${flavor} ${classes}`}
            {...props}
        >
            {children}
        </StyledButton>
    );
};
export default ButtonPublic;
