import React from "react";

const RadioModule = ({ radios = [] }) => {
  // components
  const RadioItem = ({ id = "", name = "", type = "radio", description = "", required = false }) => (
    <div className="form__radio-group">
      <input type={type} className="form__radio-input" id={id} name={name} required={required} />
      <label for={id} className="form__radio-label">
        <span className="form__radio-button"></span>
        {description}
      </label>
    </div>
  );

  return (
    <>
      {radios?.length > 0 && (
        <div className="form__group u-margin-bottom-medium">
          {radios.map((item) => (
            <RadioItem {...item} />
          ))}
        </div>
      )}
    </>
  );
};

export default RadioModule;
