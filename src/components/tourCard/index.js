import React from 'react'
import { Link } from 'react-router-dom'
import "./style.scss"

const TourCard = ({ count = 1, header = "", lis = [], path = "", linkDes = " سفارش دوره", price = "", priceOnly = "تومان" }) => {
  return (
    <div className="card">
      <div className="card__side card__side--front">
        <div className={`card__picture card__picture--${count}`}>&nbsp;</div>
        <h4 className="card__heading">
          <span className={`card__heading-span card__heading-span--${count}`}>{header}</span>
        </h4>
        <div className="card__details">
          <ul>
            {lis.map(item => <li>{item}</li>)}
          </ul>
        </div>
      </div>
      <div className={`card__side card__side--back card__side--back-${count}`}>
        <div className="card__cta">
          <div className="card__price-box">
            <p className="card__price-only">{priceOnly}</p>
            <p className="card__price-value">{price}</p>
          </div>
          <Link to={path} className="btn btn--white">
            {linkDes}
          </Link>
        </div>
      </div>
    </div>
  )
}

export default TourCard