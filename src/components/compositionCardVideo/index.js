import language from 'assets/langs'
import React, { useState } from 'react'
import "./style.scss"

const CompositionCard = ({ smallImg, largeImg, name = "p1" }) => {
  const [autoPlay, setAutoPlay] = useState(true)
  // handle
  const onMouseEnter = () => {
    setAutoPlay(true)
  }
  const onMouseLeave = () => {
    setAutoPlay(false)
  }

  return (
    <>
      {largeImg &&
        <div onMouseLeave={onMouseLeave} onMouseEnter={onMouseEnter} >
          <video className={`composition__photo composition__photo--${name}`} autoPlay={autoPlay} muted loop>
            <source src={largeImg} type="video/mp4" />
            <span>{language?.message?.videoNotSupport}</span>
          </video>
        </div>
        // <img
        //   autoPlay
        //   loop
        //   // srcset={`${smallImg} 300w, ${largeImg} 1000w`}
        //   // sizes="(max-width: 56.25em) 20vw, (max-width: 37.5em) 30vw, 300px"
        //   // alt={`img-${name}`}
        //   src={largeImg}
        // />
      }</>
  )
}

export default CompositionCard