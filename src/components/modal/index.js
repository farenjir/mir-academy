import React, { forwardRef, useState, useImperativeHandle } from "react";
import { getFromLS } from "utils/storage";
import StyledModal from "./style";

const Modals = forwardRef(
  ({
    children,
    title,
    destroyOnClose,
    maskClosable,
    className,
    reference,
    width,
    ...props
  }) => {
    const [visible, setVisible] = useState(false);
    const selectedTheme = getFromLS("theme");
    useImperativeHandle(reference, () => ({
      showModal: () => {
        
        setVisible(true);
      },
      hideModal: () => {
        setVisible(false);
      },
    }));

    const handleCancel = (e) => {
      setVisible(false);
      "onCancel" in props && props.onCancel();
    };

    return (
      <StyledModal
        selectedtheme={selectedTheme}
        title={title}
        visible={visible}
        onCancel={handleCancel}
        width={width || 1112}
        footer={null}
        destroyOnClose={destroyOnClose}
        maskClosable={maskClosable}
        className={className}
      >
        {children}
      </StyledModal>
    );
  }
);
export default Modals;
