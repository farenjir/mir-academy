import styled from "styled-components";
import { Modal } from "antd";

const StyledModal = styled(Modal)`
    ${props => props.selectedtheme && `
    &.ant-modal{
      .ant-modal-close {
        text-align: center;
        color: #888888;

        svg {
          font-size: 17px;
        }
      }
      .ant-modal-header {
        border: none;
        .ant-modal-title {
          text-align: right;
          font-weight:600;
          font-size: 20px;
          color: #444444;
          line-height: 40px;
        }
      }
      .ant-modal-footer {
        border-top: unset;
        text-align: center;
        height: 200px;
      }

      .ant-modal-wrap-rtl .ant-modal-footer {
        text-align: center;
        height: 200px;
      }
      .ant-modal-body {
        .rounded-contaier {
          padding: 30px;
          border: 1px solid #888888;
          border-radius: 3px;
          background-color: #f7f7f7;
          .ant-table {
            background: transparent;
          }
          .ant-table-tbody,
          .ant-table-tbody>tr.ant-table-placeholder:hover>td {
            background: none;
          }
          .ant-table-cell {
            background: transparent;
          }
        }
      }
    }

  `}
`;
export default (StyledModal);
