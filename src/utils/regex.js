const Regex = {
  englishAlphabets: new RegExp(/[A-Za-z\xf6-\xf8\xe5]+[A-Za-z\xf6-\xf8\xe50-9]*/),
  email: new RegExp(/^([\w.-]+)@([\w-]+)((\.(\w){2,3})+)$/),
  phoneNumber: new RegExp(/^0\d{2,3}\d{8}$/),
  mobileNumber: new RegExp(/^09[0-9]{9}$/),
  postCode: new RegExp(/^\d{10}$/),
  firstSpaceBlock: new RegExp(/^[^ ](.*|\n|\r|\r\n)*/),
  number: new RegExp(/^[0-9]*$/),
  ip: new RegExp(/\b((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\.|$)){4}\b/),
};

export default Regex;
