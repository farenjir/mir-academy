import notificationMaker from "notification";

const errorMessages = {
  UnknownError: "Unknown Error!",
  UnAuthorized: "UnAuthorized!",
  Internal: "Internal Error!",
  NotFound: "Not Found!",
  ServerError: "Server Error!",
  BadRequest: "Bad Request!",
  Forbidden: "Forbidden!",
  BadGateway: "Bad Gateway Error!",
  NetworkError: "Network Error!",
};

export const handleError = (error, message) => {
  if (message) {
    notificationMaker(message); // just show message
  } else if (!error.response) {
    notificationMaker(errorMessages.NetworkError);
  } else if (error.response.status >= 400 && error.response.status < 600) {
    switch (error.response.status) {
      case 400:
        notificationMaker(errorMessages.BadRequest);
        break;
      case 401:
        notificationMaker(errorMessages.UnAuthorized);
        break;
      case 403:
        notificationMaker(errorMessages.Forbidden);
        break;
      case 404:
        notificationMaker(errorMessages.NotFound);
        break;
      case 500:
        notificationMaker(errorMessages.ServerError);
        break;
      default:
        notificationMaker(`${error.response.status} Error`);
    }
  } else { // end
    notificationMaker(errorMessages.UnknownError);
  }
};
