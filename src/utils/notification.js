import { notification } from "antd";

const placement = "topLeft"

const notificationMaker = (message, type = "error", description) => {
  notification.config({
    placement,
    duration: 5,
  });
  return notification[type]({
    message,
    description,
  });
};

export default notificationMaker;
