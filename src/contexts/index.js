import { createContext } from "react";

const ProjectContext = createContext(null);

export const UserProvider = ProjectContext.Provider;
export const UserConsumer = ProjectContext.Consumer;

export default ProjectContext;
