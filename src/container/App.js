import React from 'react'
import { Suspense } from 'react';
import { BrowserRouter, Route, Switch } from "react-router-dom";
// redux & context
import ProjectContext from 'contexts';
// layouts
import MainLayout from 'modules/layouts/mainLayout';
// pages
import Loading from 'modules/other/loading';
import routes from 'routes';

function App() {

  return (
    <BrowserRouter>
      <Suspense fallback={<Loading />}>
        <ProjectContext.Provider value={{}}>
          <MainLayout>
            <Switch>
              {Array.isArray(routes) &&
                routes.map(
                  (route) =>
                    route.component && (
                      <Route
                        key={route?.name}
                        name={route?.name}
                        exact={route?.exact}
                        path={route?.path}
                        render={() => <route.component />}
                      />
                    ),
                )}
            </Switch>
          </MainLayout>
        </ProjectContext.Provider>
      </Suspense>
    </BrowserRouter>
  );
}

export default App;
