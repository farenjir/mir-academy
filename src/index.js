import React, { StrictMode } from 'react';
import ReactDOM from 'react-dom/client';
// redux
import store from "store";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import { persistStore } from "redux-persist";
// utils
import { setToLS } from "utils/storage";
import reportWebVitals from 'web/reportWebVitals';
// styles 
import 'antd/dist/antd.min.css';
import "assets/styles/main.scss"
// theme
import * as themes from "theme/schema.json";
// App
import App from 'container/App';

const Index = () => {
  setToLS("all-themes", themes?.default);
  return <App />;
};

const persistor = persistStore(store);

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(

  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <StrictMode>
        <Index />
      </StrictMode>
    </PersistGate>
  </Provider>,
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
