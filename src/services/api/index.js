import axios from "axios";
import { handleError } from "utils/handleErrors";

const api = async (
    addPath = "",
    method = "GET",
    data = null,
    params = {},
    backend = null,
    headers = {},
    contentType = "application/json",
    responseType = "json",
) => {
    // config api headers
    axios.defaults.headers.common["Accept-Language"] = "en";
    axios.defaults.headers.common["Api-Version"] = "1.0";
    axios.defaults.headers.common["Accept"] = "application/json";
    // get and set user token
    const token = JSON.parse(localStorage.getItem("token"));
    if (token) axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
    // config baseUrl with different backend server
    const baseUrl = process.env.REACT_APP_BACKEND_SERVER;

    return new Promise((resolve, reject) => {
        axios({
            baseURL: `${baseUrl}`,
            url: addPath,
            method,
            responseType,
            contentType,
            headers,
            params,
            data,
        })
            .then((res) => {
                if (res.data?.error?.message) {
                    handleError("showMessage", res.data.error.message);
                }
                resolve(res);
            })
            .catch((e) => {
                handleError(e);
                reject(e);
            });
    });
};

export default api;
