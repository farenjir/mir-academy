import React from "react";
// utils
import { Link } from "react-router-dom";
import language from "assets/langs";
// styles
import "./style.scss";
// components
import { ButtonModule } from "components";

export default function NotFoundPage() {
  return (
    <div className="notfound-page">
      <div className="notfound">
        <div className="notfound-404">
          <div></div>
          <h1>404</h1>
        </div>
        <h2>{language?.notFound?.notFound}</h2>
        <ButtonModule flavor="action" classes={"my-5"}>
          <Link to="/">{language?.notFound?.goBack}</Link>
        </ButtonModule>
      </div>
    </div>
  );
}
