import React from "react";
import "./style.scss";
import language from "assets/langs";

export default function Loading() {
  return (
    <div className="lds-default">
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <p>{language?.loading}</p>
    </div>
  );
}
