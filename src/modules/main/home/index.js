import React from "react";
// style
import "./style.scss";
// components
import AboutSection from "./components/AboutSection";
import FeatureSection from "./components/FeatureSection";
import StoriesSection from "./components/StoriesSection";
import TourSection from "./components/ToursSection";

const HomePage = () => {
    return (
        <>
            <AboutSection />
            <FeatureSection />
            <TourSection />
            <StoriesSection />
        </>
    );
};

export default HomePage
