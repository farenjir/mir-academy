import React from "react";
import { Link } from "react-router-dom";
// images
import video from "assets/video/video.mp4";
import img8 from "assets/images/nat-8.jpg";
import img9 from "assets/images/nat-9.jpg";
// components
import { BgVideo, StoryCard } from "components";
// descriptions
import language from "assets/langs";

const StoriesSection = () => {
    const storyList = [
        {
            figcaption: "فرشید انجیلی",
            header: "بهترین تجربه ورزشی من",
            description: language?.mockLorem2,
            imgSrc: img8,
        },
        {
            figcaption: "مریم کوچکی",
            header: "سبک زندگی مورد علاقه من",
            description: language?.mockLorem2,
            imgSrc: img9,
        },
    ];
    return (
        <section className="section-stories">
            <BgVideo video={video} />
            <div className="u-center-text u-margin-bottom-big">
                <h2 className="heading-secondary">برخی از تجربیات همراهان ما</h2>
            </div>
            {storyList.map((item) => (
                <div className="row">
                    <StoryCard {...item} />
                </div>
            ))}
            {/* <div className="u-center-text u-margin-top-huge">
                <Link to="#" className="btn-text">
                بیشتر بخوانید
                </Link>
            </div> */}
        </section>
    );
};

export default StoriesSection;
