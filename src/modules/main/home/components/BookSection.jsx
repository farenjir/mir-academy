import { FormModule, InputModule, RadioModule } from "components";
import React from "react";

const BookSection = () => {
  const formInputs = [
    {
      id: "name",
      type: "text",
      placeholder: "Full name",
      description: "Full name",
      required: true,
    },
    {
      id: "email",
      type: "email",
      placeholder: "Email address",
      description: "Email address",
      required: true,
    },
  ];
  const formRadios = [
    {
      id: "small",
      name: "size",
      type: "radio",
      description: "Small tour group",
      required: false,
    },
    {
      id: "large",
      name: "size",
      type: "radio",
      description: "Large tour group",
      required: false,
    },
  ];
  return (
    <section className="section-book">
      <div className="row">
        <div className="book">
          <FormModule title="Start booking now" submitTitle={"Next step"}>
            {formInputs.map((item) => (
              <InputModule {...item} />
            ))}
            <RadioModule radios={formRadios} />
          </FormModule>
        </div>
      </div>
    </section>
  );
};

export default BookSection;
