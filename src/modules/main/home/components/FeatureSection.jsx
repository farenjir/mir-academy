import React from "react";
import { CompassOutlined, FundOutlined, GlobalOutlined, HeartOutlined } from "@ant-design/icons";
import { FeatureBox } from "components";
import language from "assets/langs";

const FeatureSection = () => {
    const featureList = [
        {
            icon: <GlobalOutlined />,
            iconColor: "blue",
            header: "آنالیز صفر تا صد سبک زندگی",
            description: language.feature_1,
        },
        {
            icon: <HeartOutlined />,
            iconColor: "red",
            header: "جلسات مشاوره ورزشی",
            description: language.feature_5,
        },

        {
            icon: <HeartOutlined />,
            iconColor: "red",
            header: "بررسی مکمل های ورزشی و غذایی",
            description: language.feature_4,
        },
        {
            icon: <CompassOutlined />,
            iconColor: "",
            header: "طراحی برنامه های تخصصی",
            description: language.feature_2,
        },
        {
            icon: <FundOutlined />,
            iconColor: "green",
            header: "آموزش حرکات ورزشی",
            description: language.feature_3,
        },
        {
            icon: <HeartOutlined />,
            iconColor: "red",
            header: "یک سناریو آشنا ",
            description: language.feature_6,
        },
    ];

    return (
        <section className="section-features">
            <div className="row">
                {featureList.slice(0, 3).map((item) => (
                    <div className="col-1-of-3">
                        <FeatureBox {...item} />
                    </div>
                ))}
            </div>
            <div className="row">
                {featureList.slice(3, 6).map((item) => (
                    <div className="col-1-of-3">
                        <FeatureBox {...item} />
                    </div>
                ))}
            </div>
        </section>
    );
};

export default FeatureSection;
