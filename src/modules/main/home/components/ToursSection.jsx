import React from "react";
import { Link } from "react-router-dom";
import { TourCard } from "components";

const TourSection = () => {
    const cardList = [
        {
            count: 1,
            header: "دریافت برنامه تمرینی",
            lis: [
                "طول دوره 4 هفته",
                "2 ساعت مشاوره هفتگی",
                "پشتیبانی درطول دوره",
                "آنالیز هفتگی",
                "تضمین کیفیت دوره",
            ],
            path: "/",
            price: "220/000",
        },
        {
            count: 2,
            header: "دریافت برنامه میرآکادمی",
            lis: [
                "طول دوره 4 هفته",
                "3 ساعت مشاوره هفتگی",
                "پشتیبانی درطول دوره",
                "آنالیز هفتگی",
                "تضمین کیفیت دوره",
            ],
            path: "/",
            price: "490/000",
        },
        {
            count: 3,
            header: "دریافت برنامه غذایی",
            lis: [
                "طول دوره 4 هفته",
                "2 ساعت مشاوره هفتگی",
                "پشتیبانی درطول دوره",
                "آنالیز هفتگی",
                "تضمین کیفیت دوره",
            ],
            path: "/",
            price: "320/000",
        },
    ];
    return (
        <section className="section-tours" id="section-tours">
            <div className="u-center-text u-margin-bottom-big">
                <h2 className="heading-secondary">برنامه های ورزشی و تمرینی</h2>
            </div>
            <div className="row">
                {cardList.map((item, i) => (
                    <div key={++i} className="col-1-of-3">
                        {<TourCard {...item} />}
                    </div>
                ))}
            </div>
            <div className="u-center-text u-margin-top-huge">
                <Link to="/" className="btn btn--green">
                    مشاوره رایگان
                </Link>
            </div>
        </section>
    );
};

export default TourSection;
