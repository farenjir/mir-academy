import React from "react";
import { Link } from "react-router-dom";
// images
import nat_1 from "assets/images/nat-1.jpg";
import nat_2 from "assets/images/nat-2.jpg";
import nat_3 from "assets/images/nat-3.jpg";
// components
import { CompositionCard } from "components";
import language from "assets/langs";
const { home } = language.mainPages;

const AboutSection = () => {
  const imageList = [
    {
      smallImg: nat_1,
      name: "p1",
    },
    {
      smallImg: nat_2,
      name: "p2",
    },
    {
      smallImg: nat_3,
      name: "p3",
    },
  ];
  return (
    <section className="section-about">
      <div className="u-center-text u-margin-bottom-big">
        <h2 className="heading-secondary">{home?.headTitle}</h2>
      </div>

      <div className="row">
        <div className="col-1-of-2">
          <h3 className="heading-tertiary u-margin-bottom-small">روانشناسی ورزشی</h3>
          <p className="paragraph">{language?.mockLorem3}</p>

          <h3 className="heading-tertiary u-margin-bottom-small">لایف استایل کوچینگ</h3>
          <p className="paragraph">{language?.mockLorem3}</p>

          <h3 className="heading-tertiary u-margin-bottom-small">مربیگری ورزشی</h3>
          <p className="paragraph">{language?.mockLorem3}</p>

          <Link href="#" className="btn-text">
            بیشتر بخوانید
          </Link>
        </div>
        <div className="col-1-of-2">
          <div className="composition">
            {imageList.map((item) => (
              <CompositionCard {...item} />
            ))}
          </div>
        </div>
      </div>
    </section>
  );
};

export default AboutSection;
