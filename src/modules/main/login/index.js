import React from "react";
import { Link } from "react-router-dom";
// style
import "./style.scss";
import logoWhite from "assets/images/logo-white.png";
// description
import { Form } from "antd";
import { FloatLabel } from "components";
import language from "assets/langs";

const Login = () => {
    const onFinish = (values) => {
        console.log("Success:", values);
    };

    const onFinishFailed = (errorInfo) => {
        console.log("Failed:", errorInfo);
    };
    return (
        <header className="login">
            <div className="header__logo-box">
                <Link to="/">
                    <img src={logoWhite} alt="Logo" className="header__logo" />
                </Link>
            </div>
            <div className="login__box">
                <Form
                    name="basic"
                    labelCol={{
                        span: 8,
                    }}
                    wrapperCol={{
                        span: 16,
                    }}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    autoComplete="off"
                >
                    <FloatLabel label="نام کاربری" name="username" type="text" required />
                    <FloatLabel label="کلمه عبور" name="password" type="password" required />
                    <Link to="#section-tours" className="btn btn--white btn--animated mt-8 py-2">
                        ورود
                    </Link>
                </Form>
            </div>
        </header>
    );
};

export default Login;
