import React from 'react'
import { Link } from 'react-router-dom'
// style
import "./style.scss"
import fullLogo from "assets/images/logo-green-2x.png"
import smallLogo from "assets/images/logo-green-1x.png"
// description
import language from 'assets/langs'
const { mainFooter } = language.layouts


const Footer = () => {
  // footerList
  const footerList = [
    {
      name: "صفحه نخست",
      path: "/"
    },
    {
      name: "سوالات متداول",
      path: "/carers"
    },
    {
      name: "قوانین و مقررات",
      path: "/privacy"
    },
    {
      name: "درباره ما",
      path: "/contact"
    },
    {
      name: "تماس با ما",
      path: "/contact"
    }
  ]
  // components
  const FooterItem = ({ name = "", path = "" }) =>
    <li className="footer__item">
      <Link to={path} className="footer__link">
        {name}
      </Link>
    </li>

  return (
    <footer className="footer">
      <div className="footer__logo-box">
        <picture className="footer__logo">
          {/* <source
            srcset={`${smallLogo} 1x, ${fullLogo} 2x`}
            media="(max-width: 37.5em)"
          /> */}
          <img
            // srcset={`${smallLogo} 1x, ${fullLogo} 2x`}
            alt="Full logo"
            src={smallLogo}
          />
        </picture>
      </div>
      <div className="row">
        <div className="col-1-of-2">
          <div className="footer__navigation">
            <ul className="footer__list">
              {footerList.map(item => <FooterItem {...item} />)}
            </ul>
          </div>
        </div>
        <div className="col-1-of-2">
          <p className="footer__copyright">
            <span style={{ margin: "0px 5px" }}>
              {mainFooter?.builtBy}
            </span>
            <Link to="/about" className="footer__link">
              {language?.author}
            </Link>
            <br />
            {mainFooter?.describeDev}
          </p>
        </div>
      </div>
    </footer>
  )
}

export default Footer