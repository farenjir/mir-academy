import React from 'react'
import { Link } from 'react-router-dom'
// style
import "./style.scss"
import nat8 from "assets/images/nat-8.jpg"
import nat9 from "assets/images/nat-9.jpg"
// description
import language from 'assets/langs'
const { popup } = language.layouts

const Popup = () => {
  return (
    <div className="popup" id="popup">
      <div className="popup__content">
        <div className="popup__left">
          <img src={nat8} alt="Tour" className="popup__img" />
          <img src={nat9} alt="Tour" className="popup__img" />
        </div>
        <div className="popup__right">
          {/* <a href="#section-tours" className="popup__close">&times;</a> */}
          <h2 className="heading-secondary u-margin-bottom-small">{popup?.title}</h2>
          <h3 className="heading-tertiary u-margin-bottom-small">{popup?.subTitle}</h3>
          <p className="popup__text">
            {popup?.des}
          </p>
          <Link to="/" className="btn btn--green">{popup?.link}</Link>
        </div>
      </div>
    </div>
  )


}

export default Popup