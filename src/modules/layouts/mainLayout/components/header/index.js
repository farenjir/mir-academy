import React from 'react'
import { Link } from 'react-router-dom'
// style
import "./style.scss"
import logoWhite from "assets/images/logo-white.png"
// description
import language from 'assets/langs'
const { header } = language.layouts

const Header = () => {
    return (
        <header className="header">
            <div className="header__logo-box">
                <Link to="/">
                    <img src={logoWhite} alt="Logo" className="header__logo" />
                </Link>
            </div>
            <div className="header__text-box">
                <h1 className="heading-primary">
                    <span className="heading-primary--main">{header?.title}</span>
                    <span className="heading-primary--sub">{header?.subTitle}</span>
                </h1>
                <Link to="#section-tours" className="btn btn--white btn--animated">{header?.des}</Link>
            </div>
        </header>
    )
}

export default Header