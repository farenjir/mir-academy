import React from 'react'
import { Link } from 'react-router-dom'
// style
import { UserOutlined } from '@ant-design/icons'
import "./style.scss"

const Navigation = () => {
  // navList
  const navList = [
    // {
    //   name: "صفحه نخست",
    //   path: "/"
    // },
    {
      name: "سوالات متداول",
      path: "/carers"
    },
    {
      name: "قوانین و مقررات",
      path: "/privacy"
    },
    {
      name: "درباره ما",
      path: "/contact"
    }, {
      name: "تماس با ما",
      path: "/contact"
    }
  ]
  // components
  const NavItem = ({ name, path }) =>
    <li className="navigation__item">
      <Link to={path} key={name} className="navigation__link">{name}</Link>
    </li>

  return (
    <>
      <div className="navigation">
        <input type="checkbox" className="navigation__checkbox" id="navi-toggle" />
        <label for="navi-toggle" className="navigation__button">
          <span className="navigation__icon">&nbsp;</span>
        </label>
        <div className="navigation__background">&nbsp;</div>
        <nav className="navigation__nav">
          <ul className="navigation__list">
            {navList.map(item => <NavItem {...item} />)}
          </ul>
        </nav>
      </div>

      <div className="user-auth">
        <Link to={"/login"} >
          <UserOutlined className="user-auth__button" />
        </Link>
      </div>
    </>
  )
}

export default Navigation