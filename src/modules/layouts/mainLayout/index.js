import React from "react";
import { withRouter } from "react-router-dom";
// components
import Footer from "./components/footer";
import Header from "./components/header";
import Navigation from "./components/navigation";
import Popup from "./components/popup";

const MainLayout = ({ children, history }) => {
  const pathLocation = history?.location?.pathname
  const userLayout = pathLocation.includes("login") || pathLocation.includes("register")
  return (
    <>
      <Navigation />
      {!userLayout && <Header />}
      <main className="main">
        {children}
      </main>
      {!userLayout && <Footer />}
      <Popup />
    </>
  );
};

export default withRouter(MainLayout);
