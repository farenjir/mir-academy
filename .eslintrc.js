// https://www.npmjs.com/package/eslint
// https://www.npmjs.com/package/eslint-config-react-app

let configOptions = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: [
    "eslint:recommended",
    "react-app",
    // "plugin:react",
    // "plugin:react/recommended",
    // "plugin:react/jsx-runtime",
  ],
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: "latest",
    sourceType: "module",
  },
  plugins: ["react"],
  rules: {
    // "react/prop-types": "off",
    // "eqeqeq": "off",
    // "no-unused-vars": "off",
  },
};

module.exports = configOptions;
